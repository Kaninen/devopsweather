const {apiKey, lat, lon} = require('../index')

test("check there is apikey", () => {
  expect(apiKey).toBeTruthy();
});

test("check lon and lat equal length", () => {
  expect(lat.length).toEqual(lon.length);
});

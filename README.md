page_type: Demo

topic:
Demo of using CI/CD pipeline in gitlabs using kubernetes and azure for hosting the webapp.
Focusing on CI/CD, in combination with azure staging deployment.

description:
We create a website for weather where we will show we can easily update, add and change information with the help of CI/CD.
Using gitlab to auto devops pipeline to look for issues and automatically deploy. The gitlab pipeline is run on google kubernetes.
We use azure to host our website and use their deployment slotting to achieve near 100% uptime by swapping host. 

![Layout2](images/DevOpsDemoLayout.png)

Screencast: 

https://youtu.be/JwHcEaOq99I

---
languages:
- nodejs
- javascript

products:
- azure
- azure-app-service
- azure-deployment-slots
- Google Kubernetes 
---

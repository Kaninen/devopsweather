const express = require('express');
const bodyParser = require('body-parser');
const fetch = require('node-fetch');

const app = express()

const apiKey = '0c1bdf0f3482fdd29ef4e55c2ef1f7ca';

const lat = [
  66.92,
  64.59,
  63.48,
  63.15,
  61.76,
  61.00,
  60.00,
  59.27,
  59.58,
  60.25,
  59.52,
  59.13,
  58.25,
  58.05,
  56.94,
  56.94,
  57,
  56.54,
  55.87,
  56.29,
  56.66,
  57.41
];
const lon = [
  19.76,
  17.74,
  18.45,
  14.94,
  16.47,
  14.15,
  13.28,
  14.90,
  16.14,
  18.00,
  18.40,
  17.10,
  16.41,
  12.65,
  12.61,
  14.28,
  16.17,
  14.50,
  13.51,
  15.13,
  16.90,
  19
];
var temperatureInCountry = [];
var hueInCountry = [];
var invertInCountry = [];

function GetInvert(temp){
  return 80 - (31 - temp) * 3;
}
function GetHue(temp){
   return 300 + (31 - temp) * 4;
 }

app.use(express.static("public"));
app.use(bodyParser.urlencoded({ extended: true }));

app.set('view engine', 'ejs')

app.get('/', async function (req, res) {
    console.log("helllo")
  for(var i = 0; i < 22; i++){

    let url = `https://api.openweathermap.org/data/2.5/onecall?lat=${lat[i]}&lon=${lon[i]}&units=metric&appid=${apiKey}`
    let response = await fetch (url)
    let weather = await response.json()

    let temperatureInLan = parseInt(`${weather.current.temp}`);
    console.log(parseInt(temperatureInLan));
    temperatureInCountry.push(temperatureInLan);
    invertInCountry.push(GetInvert(temperatureInLan))
    hueInCountry.push(GetHue(temperatureInLan))

  }

  res.render('index', {temperature: temperatureInCountry, hue: hueInCountry, invert: invertInCountry});
})

const port = process.env.PORT || 1337;

app.listen(port, function () {
  console.log('Example app listening on port 1337!')
})

module.exports = {apiKey, lat, lon};
